# Broker demo

This is a walkthrough of how to run the Broker demo.  Scripts have been 
written in order to simplify what you need to type in order to run certain
entities.  

## Step 1

Create a bridge for cross network compatability

```bash
./setup-bridge
```

## Step 2

Create a backend controller that will be used to manage both networks

```bash
./backend-controller
```

## Step 3

Start FlowOps on Network one

```bash
./run-fo-one
cat demo.net | ../soar/scripts/gen-topo > demo.topo
cat demo.topo | ../soar/scripts/put-topo localhost:5000
```

## Step 4

Start FlowOps on Network two

```bash
./run-fo-two
cat other.net | ../soar/scripts/gen-topo > other.topo
cat other.topo | ../soar/scripts/put-topo localhost:5001
```

## Step 5

Run Mininet on Network one

```bash
./mn-one
```

## Step 6

Run Mininet on Network two

```bash
./mn-two
```

### Step 7

Run the Broker

```bash
python ../broker/broker_api.py --host localhost --port 6000
```

## Step 8

Provision Service

### Step 8a

To create a service choose two endpoint URIs.

```bash
../soar/scripts/link --no-strip-a --no-strip-b --tag-a=2 \
                     --tag-b=2 endpoint/Dolly/1 endpoint/Fay/1 \
  | ../soar/scripts/service | ../broker/scripts/connect localhost:6000
```

If a service has already been created,

```bash
cat fay-dolly.service | ../broker/scripts/connect localhost:6000
```

## Step 9

You will now be able to ping between the endpoints specified above.

# KNOWN BUGS

When the Broker sends requests to the Networks, the Networks do not get
the correct JSON - ValueError: Expected object or value.  This may be 
because of json parsing to unicode.  The Broker by itself works, and 
creates the correctly formatted Request(s).  I think it is a bug in the 
http request formatting.
